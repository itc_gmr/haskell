{-13/11/18-}
size :: Int
size = 12+8

square :: Int -> Int
square n = n*n

eligeSaludo x = if x then "adios" else "hola"

threeEqual :: Int -> Int -> Int -> Bool
threeEqual m n p = (m == n) && (n == p)

data Temp = Calor | Frio | Templado
 deriving Show
data Estacion = Primavera | Otonio | Verano | Invierno
tempNormal :: Estacion -> Temp
tempNormal Verano = Calor
tempNormal Invierno = Frio
tempNormal _ = Templado

{-14/11/18-}
{-Tipo de dato de días de la semana-}
data DiaSemana = Lunes | Martes | Miercoles | Jueves | Viernes | Sabado | Domingo
 deriving (Eq,Ord,Enum,Read,Show,Bounded)

{-Funciónes booleanas que reconoce los días labolares y los días festivos-}
diaLaborable :: DiaSemana -> Bool
diaLaborable dia = Lunes <= dia && dia <= Viernes

diaFestivo :: DiaSemana -> Bool
diaFestivo dia = dia == Sabado || dia == Domingo

{-Guardas o condicones |-}
maxMio :: Int -> Int -> Int
maxMio x y
 | x >= y = x
 | otherwise = y

maxTres :: Int -> Int -> Int -> Int
maxTres a b c
 | a >= b && b >= c = a
 | b > c && b > a = b
 | otherwise = c

{-Indice de masa corporal-}
masaCorporal :: (RealFloat a) => a -> String
masaCorporal a
 | a <= 18.5 = "Bajo peso"
 | a > 18.5 && a < 25 = "Normal"
 | a >= 25 && a < 30 = "Sobrepeso"
 | a >= 30 = "Ups"

{-Indice de masa corporal-}
horasExtra :: Int -> String
horasExtra horas
 | horas > 8 = "Oki"
 | horas > 4 = "Oks"
 | otherwise = "No cuenta"