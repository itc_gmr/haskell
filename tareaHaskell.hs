-- Definir una función que nos diga qué meses corresponden  a cada estación
-- ejemplo: Primavera == Marzo, Abril, Mayo
data Meses = Enero | Febrero | Marzo | Abril | Mayo | Junio | Julio | Agosto | Septiembre | Octubre | Noviembre | Diciembre
 deriving (Eq,Ord,Enum,Read,Show,Bounded)

data Estacion = Primavera | Verano | Otonio | Invierno
 deriving (Eq,Ord,Enum,Read,Show,Bounded)

estacion :: Estacion -> String
estacion Primavera = Marzo
estacion Primavera = Abril
estacion Primavera = Mayo
estacion Verano = Junio
estacion Verano = Julio
estacion Verano = Agosto
estacion Otonio = Septiembre
estacion Otonio = Octubre
estacion Otonio = Noviembre
estacion Invierno = Diciembre
estacion Invierno = Enero
estacion Invierno = Febrero


--Definir la función mcd, tal que (mcd a b) es el máximo común divisor de a y b calculado mediante el algoritmo de Euclides. Por ejemplo, mcd 30 45 == 15
mcd :: Integer -> Integer -> Integer

-- Calcula el factorial de un numero usando: a) guardas y b) Patrones
fact :: Integer ->  Integer

-- Decir hacia donde miras si giras 90 grados
girar90 :: Dirección -> Dirección

-- Decir que color sigue en el semaforo a partir del color actual
semaforo :: Color -> Color

-- Dice si el parametro b es multiplo del parametro a
múltiploDe :: Integer -> Integer -> Bool 

-- Retorna las raíces de una funcion en base a sus coeficientes
raíces :: Float ->    Float -> Float -> (Float, Float)

-- Funcion que reciba un numero y retorne cero
cero :: Integer -> Integer

-- Recibe la edad de una persona y le dice si puede votar
votar :: Int -> Bool

-- Dice si se presenta examen, estan excentos solo los 100 
excenta :: (Eq a, Num a) => a -> [Char]




