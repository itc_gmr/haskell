abso :: Int -> Int
abso num
 | num >= 0 = num
 | otherwise = -num

conjuncion :: Bool -> Bool -> Bool
conjuncion a b
 | a == True && b == True = True
 | a == False || b == False = False

disyuncion :: Bool -> Bool -> Bool
disyuncion a b
 | a == False && b == False = False
 | a == True || b == True = True 