--Encontrar la distancia de dos puntos en un plano cartesiano
distancia :: (Int,Int) -> (Int,Int) -> Int
distancia (x1,y1) (x2,y2) = abs((x2 - x1) + (y2 - y1))

distanciaWhere :: (Int,Int) -> (Int,Int) -> Int
distanciaWhere (x1,y1) (x2,y2) = e
 where
    e = abs((x2 - x1) + (y2 - y1))

--Encontrar los puntos que esten a la mitad de dos puntos en un plano cartesiano
puntoMedio :: (Double,Double) -> (Double,Double) -> (Double,Double)
puntoMedio (x1,y1) (x2,y2) = (((x2 + x1)/2),((y2 + y1)/2))

puntoMedioWhere :: (Double,Double) -> (Double,Double) -> (Double,Double)
puntoMedioWhere (x1,y1) (x2,y2) = (r1,r2)
 where
    r1 = ((x2 + x1)/2)
    r2 = ((y2 + y1)/2)

--Generar todos los triangulos posibles entre uno 1 y 100 del teorema de pitagoras 
-- [(a,b,c) | a <- [1..100], b <- [1..100], c <- [1..100], (c^2) == ((a^2) + (b^2))]